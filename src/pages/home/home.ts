import { Component } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { Platform, IonicPage, NavController, NavParams } from "ionic-angular";
import { Plugins } from "@capacitor/core";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  image: SafeResourceUrl;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    // private zone: NgZone,
    private sanitizer: DomSanitizer
  ) {}

  async takePicture() {
    const { Camera } = Plugins;

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: "base64"
    });
    this.image = this.sanitizer.bypassSecurityTrustResourceUrl(
      image && image.base64_data
    );
  }
}
